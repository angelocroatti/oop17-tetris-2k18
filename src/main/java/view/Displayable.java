package main.java.view;

import javafx.scene.Scene;

/**
 * Interface for a displayable class.
 */
public interface Displayable {

  /**
   * Returns the scene to display.
   */
  Scene getScene();
}
