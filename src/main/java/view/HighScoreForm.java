package main.java.view;

import static main.java.view.Dimension.HEIGHT;
import static main.java.view.Dimension.WIDTH;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.controller.FileManagement;

/**
 * class that creates the high score form and adds a new top player.
 */
public class HighScoreForm implements Displayable {
	private Scene scene;
	private VBox layout;
	
	/**Class constructor.
	  * Builds the view.
	  * @param stage
	  */
	public HighScoreForm(Stage stage, int score) {
		layout = new VBox();
		Label title = new Label("NEW HIGH SCORE");
		title.getStyleClass().addAll("title","newhighscore","-fx-font-size: 100");
		Label point = new Label(String.valueOf(score));
		Button apply = new Button("Apply");
		TextField text = new TextField();
		apply.setOnAction(e -> {
			FileManagement f = new FileManagement();
			f.writeFile(text.getText(), score);
			stage.setScene(new GameOverView(stage).getScene());
		});
		point.getStyleClass().add("sub-title-label");
		text.setMaxWidth(WIDTH.getValue() / 5);
		text.setPromptText("eg: Bazinga!");
		layout.setSpacing(HEIGHT.getValue() / 20);
		layout.setAlignment(Pos.TOP_CENTER);
		layout.setPadding(new Insets(HEIGHT.getValue() / 10,0, HEIGHT.getValue() / 3,0));
		Label msg = new Label("Insert your name!");
		layout.getChildren().addAll(title,point,msg,text,apply);
		scene = new Scene(layout, WIDTH.getValue(), HEIGHT.getValue());
		scene.getStylesheets().add("style.css");
	}
	
	public Scene getScene() {
		return scene;
	}

}
