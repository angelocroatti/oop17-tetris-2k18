package main.java.model;

import java.util.List;

public interface CollisionManager {

	/**
	 * Returns the gravity value of a given level. Gravity means the frequency at
	 * which the current tetromino moves down in Hz.
	 */
	boolean check(final List<Square> board, final Tetromino t, final int height);
}
