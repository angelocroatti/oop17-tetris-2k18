package main.java.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import main.java.model.Record;

public class FileManagement {

	private final String path = System.getProperty("user.home") 
	    +  System.getProperty("file.separator") + "scoreboard.txt";
	private final List<Record> list;
	private static final int NUMBEROFROWS = 10;
	private final File file;

	/**
	 * 
	 * Costruttor.
	 */
	public FileManagement() {
		file = new File(path);
		list = new ArrayList<Record>(Collections.emptyList());
		readFile();
	}

	/**
	 * writes the values of the list inside the file.
	 * @param namePlayer
	 * @param score
	 * 
	 */
	public void writeFile(final String namePlayer, final Integer score) {
		this.list.add(new Record(namePlayer, score));
		this.list.sort((a, b) -> b.getScore() - a.getScore());
		if (list.size() >= NUMBEROFROWS) {
			this.list.remove(list.size() - 1);
		}
		try (BufferedWriter bw = new BufferedWriter(
		        new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
			for (Record r : list) {
				bw.write(r.getName());
				bw.newLine();
				bw.write(String.valueOf(r.getScore()));
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * Returns the list with the values in the file.
	 */
	public List<Record> listCurrent() {
		return Collections.unmodifiableList(list);
	}

	/**
	 * Reads the file and modify the values in the list.
	 * 
	 */
	private void readFile() {
		String name;
		String score;
		if (file.exists()) {
			try (BufferedReader br = new BufferedReader(
			        new InputStreamReader(new FileInputStream(file)))) {
				while ((name = br.readLine()) != null) {
					if ((score = br.readLine()) != null) {
						try {
							list.add(new Record(
							    name, Integer.parseInt(score)));
						} catch (Exception e) {
						  System.out.println(
						      "Lettura punti sbalgliata" + score);
						}
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			list.sort((a, b) -> b.getScore() - a.getScore());
		}

	}

}
